export interface Item {
  id: number;
  created_at: string;
  author: string;
  title?: string;
  url: string;
  text: string;
  points: number;
  parent_id?: number;
  children: Item[];
}

export interface FirstItem {
  author: string;
  title?: string;
  text: string;
}

export interface ItemFlatNode {
  author: string;
  text: string;
  expandable: boolean;
  level: number;
}
