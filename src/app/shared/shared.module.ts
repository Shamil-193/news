import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsService } from './services/news.service';
import { ItemsService } from './services/items.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    NewsService,
    ItemsService,
  ],
})
export class SharedModule {
}
