import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DecoratedNews } from '../interfaces/news';
import { environment } from '../../../environments/environment';

@Injectable()
export class NewsService {
  constructor(private http: HttpClient) {
  }

  getWithQuery(page: number = 0, perPage: number = 30, query: string = 'front_page'): Observable<DecoratedNews> {
    return this.http.get<DecoratedNews>(`${environment.hnUrl}/search?tags=${query}&hitsPerPage=${perPage}&page=${page}`);
  }
}
