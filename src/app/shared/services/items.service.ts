import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Item } from '../interfaces/item';

@Injectable()
export class ItemsService {
  constructor(private http: HttpClient) {
  }

  getById(id: string): Observable<Item> {
    return this.http.get<Item>(`${environment.hnUrl}/items/${id}`);
  }
}
