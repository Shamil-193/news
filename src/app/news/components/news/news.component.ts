import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { News } from '../../../shared/interfaces/news';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsComponent {
  @Input() news: News[] | undefined;
  @Input() currentPage: number;
}
