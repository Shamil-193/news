import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ItemsService } from '../../../shared/services/items.service';
import { BehaviorSubject, catchError, finalize, first, switchMap, tap, throwError } from 'rxjs';
import { FirstItem, Item, ItemFlatNode } from '../../../shared/interfaces/item';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { FlatTreeControl } from '@angular/cdk/tree';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-comment-page',
  templateUrl: './comment-page.component.html',
  styleUrls: ['./comment-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CommentPageComponent implements OnInit {

  error$ = new BehaviorSubject<string | null>(null);

  firstItem: FirstItem;
  loading = false;

  flatTreeControl = new FlatTreeControl<ItemFlatNode>(
    node => node.level,
    node => node.expandable,
  );

  treeFlattener = new MatTreeFlattener((node: Item, level: number): ItemFlatNode => {
      return {
        author: node.author,
        text: node.text,
        expandable: node.children?.length > 0,
        level,
      };
    },
    node => node.level,
    node => node.expandable,
    node => node.children,
  );

  flatDataSource = new MatTreeFlatDataSource(this.flatTreeControl, this.treeFlattener);

  constructor(
    private route: ActivatedRoute,
    private itemsService: ItemsService,
    private changeDetector: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.loadComments();
  }

  private loadComments(): void {
    this.loading = true;
    this.route.params.pipe(
      switchMap((param: Params) => this.itemsService.getById(param['id'])
        .pipe(
          first(),
          tap(({author, title = '', text, children}) => {
            this.firstItem = {author, title, text};
            this.flatDataSource.data = children;
          }),
          catchError((err: HttpErrorResponse | string) => {
            this.error$.next('Что-то пошло не так...');
            console.log('error', err);
            return throwError(err);
          }),
          finalize(() => {
            this.loading = false;
            this.changeDetector.detectChanges();
          }),
        ),
      ),
    ).subscribe();
  }

  hasFlatChild(index: number, node: ItemFlatNode) {
    return node.expandable;
  }

}
