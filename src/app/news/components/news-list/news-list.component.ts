import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { BehaviorSubject, catchError, finalize, first, Observable, Subject, takeUntil, tap, throwError } from 'rxjs';
import { DecoratedNews } from '../../../shared/interfaces/news';
import { NewsService } from '../../../shared/services/news.service';
import { MatPaginator } from '@angular/material/paginator';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsListComponent implements OnInit, AfterViewInit, OnDestroy {

  decoratedNews$: Observable<DecoratedNews>;
  error$ = new BehaviorSubject<string | null>(null);
  protected componentDestroy$: Subject<void> = new Subject<void>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  loading = false;
  length: number;
  pageSize: number;

  constructor(private newsService: NewsService,
              private changeDetector: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.loadNewsPage();
  }

  ngAfterViewInit(): void {
    this.paginator?.page.pipe(
      takeUntil(this.componentDestroy$),
      tap(() => this.loadNewsPage()),
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.componentDestroy$.next();
    this.componentDestroy$.complete();
  }

  private loadNewsPage(): void {
    this.loading = true;
    this.decoratedNews$ = this.newsService.getWithQuery(this.paginator?.pageIndex ?? 0)
      .pipe(
        first(),
        tap((decoratedNews: DecoratedNews) => {
          this.length = decoratedNews.nbHits;
          this.pageSize = decoratedNews.hitsPerPage;
        }),
        catchError((err: HttpErrorResponse) => {
          this.error$.next('Что-то пошло не так...');
          console.log('error', err);
          return throwError(err);
        }),
        finalize(() => {
          this.loading = false;
          this.changeDetector.detectChanges();
        }),
      );
  }

}
